.POSIX:
.SUFFIXES:

CC      = gcc
#CFLAGS  = -std=c99 -pedantic-errors -Wall -Wextra -O3 -DNDEBUG -D_POSIX_C_SOURCE=200809L
CFLAGS  = -std=c99 -pedantic-errors -Wall -Wextra -O0 -g -D_POSIX_C_SOURCE=200809L `sdl2-config --cflags`
#LDFLAGS = -static
LDFLAGS =
LDLIBS  = `sdl2-config --libs`

all: main.x

main: main.x
	./$< 'groov.mod'

clean:
	rm -rf *.o *.x

.SUFFIXES: .c .o .x

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.o.x:
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)
