#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>

#define MAXCHNS 8
#define MAXPATS 128

#define ARRLEN(ARR) (sizeof(ARR) / sizeof(*ARR))
#define ECHG16(VAL) ((((VAL) & 0xFFU) << 8U) & 0xFF00U) | ((((VAL) & 0xFF00U) >> 8U) & 0xFFU)
#define AMTOHZ(PRD) (7159090.5 / ((PRD) * 2))

typedef struct {
	uint16_t effs[MAXCHNS][64];
	uint8_t  pidxs[MAXCHNS][64], snums[MAXCHNS][64];
} Pat;

typedef struct {
	uint16_t len, llen, lstrt;
	uint8_t  ftune, vol;
	int8_t   *dat;
	char     name[22];
} Samp;

static int  modload(FILE *file);
static void modunload(void);
static int  sinit(void);
static void sndcb(void *udat, Uint8 *strm, int len);
static void sterm(void);

static Pat               pats[MAXPATS];
static Samp              samps[31];
static SDL_AudioDeviceID dev;
static size_t            nch;
static uint8_t           pnums, ords[128], slen;
static char              sname[20];

static const uint16_t prdlks[16][36] = {
	{
		856, 808, 762, 720, 678, 640, 604, 570, 538, 508, 480, 453,
		428, 404, 381, 360, 339, 320, 302, 285, 269, 254, 240, 226,
		214, 202, 190, 180, 170, 160, 151, 143, 135, 127, 120, 113
	},{
		850, 802, 757, 715, 674, 637, 601, 567, 535, 505, 477, 450,
		425, 401, 379, 357, 337, 318, 300, 284, 268, 253, 239, 225,
		213, 201, 189, 179, 169, 159, 150, 142, 134, 126, 119, 113
	},{
		844, 796, 752, 709, 670, 632, 597, 563, 532, 502, 474, 447,
		422, 398, 376, 355, 335, 316, 298, 282, 266, 251, 237, 224,
		211, 199, 188, 177, 167, 158, 149, 141, 133, 125, 118, 112
	},{
		838, 791, 746, 704, 665, 628, 592, 559, 528, 498, 470, 444,
		419, 395, 373, 352, 332, 314, 296, 280, 264, 249, 235, 222,
		209, 198, 187, 176, 166, 157, 148, 140, 132, 125, 118, 111
	},{
		832, 785, 741, 699, 660, 623, 588, 555, 524, 495, 467, 441,
		416, 392, 370, 350, 330, 312, 294, 278, 262, 247, 233, 220,
		208, 196, 185, 175, 165, 156, 147, 139, 131, 124, 117, 110
	},{
		826, 779, 736, 694, 655, 619, 584, 551, 520, 491, 463, 437,
		413, 390, 368, 347, 328, 309, 292, 276, 260, 245, 232, 219,
		206, 195, 184, 174, 164, 155, 146, 138, 130, 123, 116, 109
	},{
		820, 774, 730, 689, 651, 614, 580, 547, 516, 487, 460, 434,
		410, 387, 365, 345, 325, 307, 290, 274, 258, 244, 230, 217,
		205, 193, 183, 172, 163, 154, 145, 137, 129, 122, 115, 109
	},{
		814, 768, 725, 684, 646, 610, 575, 543, 513, 484, 457, 431,
		407, 384, 363, 342, 323, 305, 288, 272, 256, 242, 228, 216,
		204, 192, 181, 171, 161, 152, 144, 136, 128, 121, 114, 108
	},{
		907, 856, 808, 762, 720, 678, 640, 604, 570, 538, 508, 480,
		453, 428, 404, 381, 360, 339, 320, 302, 285, 269, 254, 240,
		226, 214, 202, 190, 180, 170, 160, 151, 143, 135, 127, 120
	},{
		900, 850, 802, 757, 715, 675, 636, 601, 567, 535, 505, 477,
		450, 425, 401, 379, 357, 337, 318, 300, 284, 268, 253, 238,
		225, 212, 200, 189, 179, 169, 159, 150, 142, 134, 126, 119
	},{
		894, 844, 796, 752, 709, 670, 632, 597, 563, 532, 502, 474,
		447, 422, 398, 376, 355, 335, 316, 298, 282, 266, 251, 237,
		223, 211, 199, 188, 177, 167, 158, 149, 141, 133, 125, 118
	},{
		887, 838, 791, 746, 704, 665, 628, 592, 559, 528, 498, 470,
		444, 419, 395, 373, 352, 332, 314, 296, 280, 264, 249, 235,
		222, 209, 198, 187, 176, 166, 157, 148, 140, 132, 125, 118
	},{
		881, 832, 785, 741, 699, 660, 623, 588, 555, 524, 494, 467,
		441, 416, 392, 370, 350, 330, 312, 294, 278, 262, 247, 233,
		220, 208, 196, 185, 175, 165, 156, 147, 139, 131, 123, 117
	},{
		875, 826, 779, 736, 694, 655, 619, 584, 551, 520, 491, 463,
		437, 413, 390, 368, 347, 328, 309, 292, 276, 260, 245, 232,
		219, 206, 195, 184, 174, 164, 155, 146, 138, 130, 123, 116
	},{
		868, 820, 774, 730, 689, 651, 614, 580, 547, 516, 487, 460,
		434, 410, 387, 365, 345, 325, 307, 290, 274, 258, 244, 230,
		217, 205, 193, 183, 172, 163, 154, 145, 137, 129, 122, 115
	},{
		862, 814, 768, 725, 684, 646, 610, 575, 543, 513, 484, 457,
		431, 407, 384, 363, 342, 323, 305, 288, 272, 256, 242, 228,
		216, 203, 192, 181, 171, 161, 152, 144, 136, 128, 121, 114
	}
};

static int
modload(FILE *const file)
{
	size_t i, j, k, l;
	uint8_t buf[4];
	char sig[4];
	assert(file != NULL);
	for (i = 0; i < ARRLEN(samps); ++i)
		samps[i].dat = NULL;
	if (fseek(file, 1080, SEEK_SET) != 0) {
		fprintf(stderr, "ERROR: failed to seek to magic signature.\n");
		return -1;
	}
	if (fread(sig, sizeof(*sig), ARRLEN(sig), file) < ARRLEN(sig)) {
		fprintf(stderr, "ERROR: failed to read magic signature.\n");
		return -1;
	}
	if (memcmp(sig, "M.K.", 4) == 0) {
		nch = 4;
	} else if (memcmp(sig, "6CHN", 4) == 0) {
		nch = 6;
	} else if (memcmp(sig, "8CHN", 4) == 0) {
		nch = 8;
	} else {
		fprintf(stderr, "ERROR: invalid magic signature.\n");
		return -1;
	}
	if (fseek(file, 0, SEEK_SET) != 0) {
		fprintf(stderr, "ERROR: failed to seek to beginning of file.\n");
		return -1;
	}
	if (fread(sname, sizeof(*sname), ARRLEN(sname), file) < ARRLEN(sname)) {
		fprintf(stderr, "ERROR: failed to read song name.\n");
		return -1;
	}
	sname[ARRLEN(sname) - 1] = '\0';
	for (i = 0; i < ARRLEN(samps); ++i) {
		if (fread(samps[i].name, sizeof(*samps[i].name), ARRLEN(samps[i].name), file) < ARRLEN(samps[i].name)) {
			fprintf(stderr, "ERROR: failed to read sample name.\n");
			return -1;
		}
		if (fread(&samps[i].len, sizeof(samps[i].len), 1, file) < 1) {
			fprintf(stderr, "ERROR: failed to read sample length.\n");
			return -1;
		}
		samps[i].len = ECHG16(samps[i].len);
		if (fread(&samps[i].ftune, sizeof(samps[i].ftune), 1, file) < 1) {
			fprintf(stderr, "ERROR: failed to read sample fine tune.\n");
			return -1;
		}
		if (fread(&samps[i].vol, sizeof(samps[i].vol), 1, file) < 1) {
			fprintf(stderr, "ERROR: failed to read sample vol tune.\n");
			return -1;
		}
		if (fread(&samps[i].lstrt, sizeof(samps[i].lstrt), 1, file) < 1) {
			fprintf(stderr, "ERROR: failed to read sample loop start.\n");
			return -1;
		}
		samps[i].lstrt = ECHG16(samps[i].lstrt);
		if (fread(&samps[i].llen, sizeof(samps[i].llen), 1, file) < 1) {
			fprintf(stderr, "ERROR: failed to read sample loop length.\n");
			return -1;
		}
		samps[i].llen = ECHG16(samps[i].llen);
		if (samps[i].llen == 1)
			samps[i].llen = 0;
	}
	if (fread(&slen, sizeof(slen), 1, file) < 1) {
		fprintf(stderr, "ERROR: failed to read song length.\n");
		return -1;
	}
	if (fseek(file, 1, SEEK_CUR) != 0) {
		fprintf(stderr, "ERROR: failed to seek after read of song length.\n");
		return -1;
	}
	if (fread(ords, sizeof(*ords), ARRLEN(ords), file) < ARRLEN(ords)) {
		fprintf(stderr, "ERROR: failed to read song orders.\n");
		return -1;
	}
	pnums = 0;
	for (i = 0; i < slen; ++i)
		if (ords[i] > pnums)
			pnums = ords[i];
	++pnums;
	if (fseek(file, 4, SEEK_CUR) != 0) {
		fprintf(stderr, "ERROR: failed to seek after read of orders.\n");
		return -1;
	}
	for (i = 0; i < pnums; ++i) {
		for (j = 0; j < 64; ++j) {
			for (k = 0; k < nch; ++k) {
				if (fread(buf, sizeof(*buf), ARRLEN(buf), file) < ARRLEN(buf)) {
					fprintf(stderr, "ERROR: failed to read pattern data.\n");
					return -1;
				}
				pats[i].snums[k][j] = (buf[0] & 0xF0) | (((buf[2] & 0xF0) >> 4U) & 0xFU);
				pats[i].pidxs[k][j] = 0xFFU;
				for (l = 0; l < ARRLEN(prdlks[0]); ++l)
					if (prdlks[0][l] == ((((buf[0] & 0xFU) << 8U) & 0xF00U) | buf[1]))
						pats[i].pidxs[k][j] = l;
				pats[i].effs[k][j] = (((buf[2] & 0xFU) << 8U) & 0xF00U) | buf[3];
			}
		}
	}
	for (i = 0; i < ARRLEN(samps); ++i) {
		if ((samps[i].dat = malloc(sizeof(*samps[i].dat) * samps[i].len * 2)) == NULL) {
			fprintf(stderr, "ERROR: failed to allocate memory for sample data for sample %zu.\n", i);
			return -1;
		}
		if (fread(samps[i].dat, sizeof(*samps[i].dat), samps[i].len * 2, file) < (samps[i].len * 2)) {
			fprintf(stderr, "ERROR: failed to read sample data for sample %zu.\n", i);
			return -1;
		}
	}
#ifndef NDEBUG
	printf("nch: %zu\n", nch);
	printf("sname: %s\n", sname);
	for (i = 0; i < ARRLEN(samps); ++i) {
		printf("samps[%zu]:\n", i);
		printf("\tname: %s\n", samps[i].name);
		printf("\tlen: %4.4x\n", samps[i].len * 2);
		printf("\tftune: %2.2x\n", samps[i].ftune);
		printf("\tfvol: %2.2x\n", samps[i].vol);
		printf("\tlstrt: %4.4x\n", samps[i].lstrt * 2);
		printf("\tllen: %4.4x\n", samps[i].llen * 2);
	}
	printf("slen: %2.2x\n", slen);
	for (i = 0; i < slen; ++i)
		printf("ords[%zu]: %2.2x\n", i, ords[i]);
	printf("pnums: %2.2x\n", pnums);
	for (i = 0; i < slen; ++i) {
		for (j = 0; j < 64; ++j) {
			printf("%2.2x: ", j);
			for (k = 0; k < nch; ++k) {
				printf("%3.3x %2.2x %4.4x |", pats[ords[i]].pidxs[k][j], pats[ords[i]].snums[k][j], pats[ords[i]].effs[k][j]);
			}
			printf("\n");
		}
		printf("\n");
	}
#endif /* NDEBUG */
	return 0;
}

static void
modunload(void)
{
	size_t i;
	for (i = 0; i < ARRLEN(samps); ++i)
		if (samps[i].dat != NULL)
			free(samps[i].dat);
}

static int
sinit(void)
{
	SDL_AudioSpec des, obt;
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		fprintf(stderr, "ERROR: %s.\n", SDL_GetError());
		return -1;
	}
	SDL_memset(&des, 0, sizeof(des));
	des.freq = 48000;
	des.format = AUDIO_S8;
	des.channels = 1;
	des.samples = 4096;
	des.callback = sndcb;
	if ((dev = SDL_OpenAudioDevice(NULL, 0, &des, &obt, 0)) == 0) {
		fprintf(stderr, "ERROR: %s.\n", SDL_GetError());
		return -1;
	}
	return 0;
}

static void
sndcb(void *udat, Uint8 *strm, int len)
{
	size_t i;
	for (i = 0; i < len; ++i)
		((Sint8 *)strm)[i] = 0;
}

static void
sterm(void)
{
	SDL_CloseAudioDevice(dev);
	SDL_Quit();
}

int
main(int argc, char *argv[])
{
	FILE *f;
	int rc;
	if (argc != 2) {
		printf("Usage: ./main.x [mod]\n");
		printf("\n\tmod - The MOD file to play.\n");
		return EXIT_FAILURE;
	}
	if ((f = fopen(argv[1], "rb")) == NULL) {
		fprintf(stderr, "ERROR: failed to open mod file %s.\n", argv[1]);
		return EXIT_FAILURE;
	}
	if (modload(f) < 0) {
		fclose(f);
		return EXIT_FAILURE;
	}
	fclose(f);
	rc = EXIT_SUCCESS;
	if (sinit() < 0) {
		rc = EXIT_FAILURE;
		goto end;
	}
end:
	sterm();
	modunload();
	return rc;
}
